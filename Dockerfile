FROM python:3.6.5

COPY requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt

RUN useradd --shell /bin/bash --create-home patrick

USER patrick

RUN mkdir /home/patrick/src
WORKDIR /home/patrick/src

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

VOLUME ["/home/patrick/data"]
VOLUME ["/home/patrick/src"]

COPY --chown=patrick:users src /home/patrick/src
COPY --chown=patrick:users data /home/patrick/data

COPY --chown=patrick:users docker-entrypoint.sh ./ 
CMD [ "./docker-entrypoint.sh" ]
