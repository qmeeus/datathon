#!/bin/sh

if [ -z "$PORT" ]; then
    export PORT=8050
fi
echo $PORT
exec python dashboard.py
