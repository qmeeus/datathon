import base64
import json

mapdata = json.load(open('europe-map.geo.json', 'r'))
DATA_URL_PREFIX = 'data:application/json;base64,'

countries_json = {}
for country in mapdata['features']:
    name = country['properties']['name']
    country_geojson_content = dict(
        type='FeatureCollection',
        features=[country]
    )
    encoded = base64.b64encode(
        json.dumps(country_geojson_content).encode('utf-8')
    ).decode('utf-8')
    data_url = f'{DATA_URL_PREFIX}{encoded}'
    countries_json[name] = data_url

json.dump(countries_json, open('country_dataurls.json', 'w'))
