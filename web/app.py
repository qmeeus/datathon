import json

import dash
import dash_auth
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from plotly.graph_objs import layout as go_layout
import plotly
from plotly import graph_objs as go

# Keep this out of source code repository - save in a file or a database
VALID_USERNAME_PASSWORD_PAIRS = [
    ['hello', 'world']
]

# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
external_stylesheets = ["https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
                "//fonts.googleapis.com/css?family=Raleway:400,300,600",
                "//fonts.googleapis.com/css?family=Dosis:Medium",
                "https://cdn.rawgit.com/plotly/dash-app-stylesheets/62f0eb4f1fadbefea64b2404493079bf848974e8/dash-uber-ride-demo.css",
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"]
mapbox_access_token = 'pk.eyJ1IjoiYWxpc2hvYmVpcmkiLCJhIjoiY2ozYnM3YTUxMDAxeDMzcGNjbmZyMmplZiJ9.ZjmQ0C2MNs1AzEBC_Syadg'

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)

app.layout = html.Div([
    # dcc.Dropdown(
    #     id='dropdown',
    #     options=[{'label': i, 'value': i} for i in ['A', 'B']],
    #     value='A'
    # ),
    # dcc.Graph(id='graph')
    dcc.Dropdown(
        id='my-dropdown',
        options=[
            {'label': 'April 2014', 'value': 'Apr'},
            {'label': 'May 2014', 'value': 'May'},
            {'label': 'June 2014', 'value': 'June'},
            {'label': 'July 2014', 'value': 'July'},
            {'label': 'Aug 2014', 'value': 'Aug'},
            {'label': 'Sept 2014', 'value': 'Sept'}
        ],
        value="Apr",
        placeholder="Please choose a month",
        className="month-picker"
    ),
    dcc.Graph(id='map-graph'),
], className='container')

# @app.callback(
#     dash.dependencies.Output('graph', 'figure'),
#     [dash.dependencies.Input('dropdown', 'value')])
# def update_graph(dropdown_value):
#     return {
#         'layout': {
#             'title': 'Graph of {}'.format(dropdown_value),
#             'margin': {
#                 'l': 20,
#                 'b': 20,
#                 'r': 10,
#                 't': 60
#             }
#         },
#         'data': [{'x': [1, 2, 3], 'y': [4, 1, 2]}]
#     }
layers = []
countries_mapdata = json.load(open('country_dataurls.json', 'r'))
# For testing purposes
import random
for country, data_url in countries_mapdata.items():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    layers.append(dict(
        sourcetype = 'geojson',
        source = data_url,
        type = 'fill',
        color = f'rgba({r},{g},{b},0.8)'
    ))

@app.callback(Output("map-graph", "figure"),
    [Input("my-dropdown", "value")],
    [State('map-graph', 'relayoutData')])
def update_graph(a, b):
    data = [
        go.Scattermapbox(
        )
    ]

    layout = go.Layout(
        autosize=True,
        # height=550,
        margin=go_layout.Margin(l=0, r=0, t=0, b=0),
        hovermode='closest',
        mapbox=dict(
            layers=layers,
            style='dark',
            accesstoken=mapbox_access_token,
            bearing=0,
            center=dict(
                lat=53.8097232,
                lon=1.6563332
            ),
            pitch=0,
            zoom=2.5
        ),
    )

    fig = dict(data=data, layout=layout)

    return go.Figure(**fig)

  # ]

  # layout = go.Layout(
  #     autosize=True,
  #     hovermode='closest',
  #     mapbox=dict(
  #         accesstoken=mapbox_access_token,
  #         bearing=0,
  #         center=dict(
  #             lat=38.92,
  #             lon=-77.07
  #         ),
  #         pitch=0,
  #         zoom=10
  #     ),
  # )

  # fig = dict(data=data, layout=layout)

app.scripts.config.serve_locally = True


if __name__ == '__main__':
    app.run_server(debug=True)
