import functools
import os.path as p
import pandas as pd
import numpy as np
import pickle

from utils import absolute_path

class Data:

    DATA_FILE = absolute_path("../data/chronicgdayconsumers.xlsx")
    DATA_PICKLE = absolute_path('../data/chronicgdayconsumers.pkl')
    INDICATORS_DATA_FILE = absolute_path("../data/indicators.xlsx")

    
    def __init__(self):
        self.dataframes = self.load_data()
        self.indicators = self.load_indicators_data()

    def load_data(self):
        print("Loading data...")
        if p.exists(self.DATA_PICKLE):
            with open(self.DATA_PICKLE, 'rb') as fp:
                dataframes = pickle.load(fp)
        elif p.exists(self.DATA_FILE):
            dataframes = pd.read_excel(
                self.DATA_FILE, 
                sheet_name=[self.get_sheet_name(i) for i in range(1,5)], 
                skiprows=[0,1], 
                header=[0])
        else:
            raise FileNotFoundError
        return dataframes

    def load_indicators_data(self):
        print("Loading indicators data...")
        return pd.read_excel(
            self.INDICATORS_DATA_FILE,
            sheet_name="SupportingFactors",
            header=[0])
            
    def check_level_nr(self, level_nr):
        assert 0 < level_nr < len(self.dataframes) + 1

    def get_level_column(self, level_nr):
        self.check_level_nr(level_nr)
        return "Foodex L{}".format(level_nr)

    def get_sheet_name(self, level_nr):
        return "L{}_Consumers_only_g_day".format(level_nr)

    def get_sheet_by_id(self, level_nr):
        return self.dataframes[self.get_sheet_name(level_nr)]

    def get_categories(self):
        cat_names = \
            sorted(set([c for df in self.dataframes.values() 
            for c in df.columns if c.startswith("Foodex")]))   
        
        cats = \
            [df.drop([t for t in df.columns if t not in cat_names], axis=1).drop_duplicates() 
            for df in self.dataframes.values()]

        categories = cats[-1]
        for i in range(2,0,-1):
            c = cat_names[i]
            categories = categories.set_index(c).join(cats[i].set_index(c)).reset_index()

        return categories[cat_names]

    def get_categories_as_dict(self):
        df = self.get_categories()

    def get_country_mean(self, level_id, category):
        def mean(g):
            Z = (g['Nr Consumers'] / g['% Consumers']).sum()
            return (g["Mean"] * (g['Nr Consumers'] / g['% Consumers']) / Z).sum()

        filter_dict = {self.get_level_column(level_id): category}
        df = self.df_filter(self.get_sheet_by_id(level_id), filter_dict)
        df = df.groupby("Country").apply(mean).sort_values()
        return list(df.index), list(df.values)

    def get_country_names(self):
        return list(self.get_sheet_by_id(1).Country.unique())

    def get_country_indicators(self, country):
        df = self.indicators.set_index("country")
        return df.loc[country].to_dict()

    def get_mean_by_country_cat(self, country, foodex, cat_level):
        local = self.get_sheet_by_id(cat_level).copy()
        local = local.loc[(local['Country']==country) & (local['Foodex L{}'.format(cat_level)]==foodex)]
        local['actual'] = local['Nr Consumers']/local['% Consumers']
        return (local['actual']/local['actual'].sum()*local['Mean']).sum()
        
    def get_country_mean_df(self, level_id, category):
        def mean(g):
            Z = (g['Nr Consumers'] / g['% Consumers']).sum()
            return (g["Mean"] * (g['Nr Consumers'] / g['% Consumers']) / Z).sum()

        filter_dict = {self.get_level_column(level_id): category}
        df = self.df_filter(self.get_sheet_by_id(level_id), filter_dict)
        df = df.groupby("Country").apply(mean).sort_values()
        return df

    def get_country_top_cat(self, country, top=5):
        cats = list(self.get_sheet_by_id(1)["Foodex L1"].unique())
        countries = list(self.get_sheet_by_id(1)["Country"].unique())
        c_means = {}
        for cat in cats:
            df = self.get_country_mean_df(1, cat)
            mean_v = df.mean()
            min_v = df.min()
            max_v = df.max()
            dict = df.to_dict()

            if country in dict.keys():
                value = dict[country]
                c_means[cat] = (value - mean_v) / (max_v - min_v)
    
        c_means = {k: v for k, v in sorted(c_means.items(), key=lambda kv: kv[1], reverse=True)}
        top_c_means = {k: c_means[k] for k in list(c_means)[:top]}
        return top_c_means


    @staticmethod
    def df_filter(df, filter_dict):
        df = df.copy()
        for column, values in filter_dict.items():
            assert column in df.columns, "Column not found: {}".format(column)
            values = values if type(values) == list else [values]
            df = df.loc[df[column].isin(values)]
            assert len(df) > 1, "No values left: {}".format(values)
        return df

    @functools.lru_cache(maxsize=20, typed=False)
    def get_country_indicators(self, country):
        df = self.indicators.set_index("country")
        return df.loc[country].to_dict()
        
    def load_kmeans(self, k):
        with open("../data/kmeans{}.pkl".format(k), 'rb') as f:
            return pickle.load(f)
        
    def get_kmeans_country_label(self, k):
        clusters = self.load_kmeans(k)
        return list(clusters['country']), list(clusters['label'])

        
data = Data()
