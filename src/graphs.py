from utils import load_json
from plotly import graph_objs as go
from plotly.graph_objs import layout as go_layout
from utils import load_text
from data import data
from plotly import tools

cluster_colors = ["#D07A44", "#AE5663", "#89C0BC", "#3A8994", "#3A8994"]


def build_map(country_colors):
    layers = []
    countries_mapdata = load_json("resources/country_dataurls.json")
    for c, color in country_colors.items():
        layers.append(dict(
            sourcetype = 'geojson',
            source = countries_mapdata[c],
            type = 'fill',
            color = color))
    return layers

def update_barh(level1_value, level2_value):
    category = level2_value if level2_value else level1_value
    level = 2 if level2_value else 1

    y_values, x_values = data.get_country_mean(level, category)

    max_mean = max(x_values)
    x_norm = [(m * 1.0) / max_mean for m in x_values]
    country_colors = {}
    bar_colors = []
    for m in x_norm:
        col_r = int(m * 255)
        col_g = int(m * 200)+55
        col_b = int(m * 200)+55
        bar_colors.append(f'rgba({col_r},{col_g},{col_b},0.8)')

    chart_data = [go.Bar(
        x=x_values,
        y=y_values,
        text=list(map("{:.0f}".format, x_values)),
        textposition="auto",
        orientation="h",
        marker=dict(color=list(reversed(bar_colors)))),]
        
    layout = dict(
        autosize=True, 
        template='plotly_dark',
        height=500,
        margin=go.layout.Margin(l=0, r=0, b=20, t=20, pad=4),
        xaxis = dict(hoverformat = '.2f'),
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)')

    return go.Figure(data=chart_data, layout=layout)

def update_country_indicators(country_name):
    if country_name is not None:
        country_data = data.get_country_indicators(country_name)
        print(country_data)
    layout = dict(
        height=10,
        template='plotly_dark',
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)')
    return go.Figure(data=[], layout=layout)

def get_clusters(clusters, k):
    cluster_list = []
    for i in range(k):
        cluster = clusters.loc[clusters['label']==i]
        c = dict(
            name = "y{}".format(i),
            opacity = 0.1,
            type = "mesh3d",    
            x=cluster[0],y=cluster[1],z=cluster[2])
        cluster_list.append(c)
    return cluster_list

def get_kmeans_figure(n_clusters):
    clusters = data.load_kmeans(n_clusters)
    colors = clusters['label'].map(lambda i: cluster_colors[i])
    trace = go.Scatter3d(
            x=clusters[0],y=clusters[1],z=clusters[2],
            mode='markers+text',
            text=clusters["country"],
            showlegend=False,
            marker=dict(color=colors, line=dict(width=1, color='black')))

    c = get_clusters(clusters, n_clusters)
    fig_data = [trace]
    fig_data.extend(c)

    layout = dict(
        height=600,
        margin=go.layout.Margin(l=10, r=10, b=60, t=10, pad=4),
        template='plotly_dark',
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)')
    
    return go.Figure(data=fig_data, layout=layout)


def meta_graph(*countries):
    countries = [c for c in countries if c is not None]
    indicators = [data.get_country_indicators(c) for c in countries]

    happiness_score = go.Bar(
        x=countries, #should look for flag / abbreviation / color coding
        y=[i['Happiness score'] for i in indicators],
    )
    # RGBA(208, 122, 68, 1)
    gdp = go.Bar(
        x=countries, #should look for flag / abbreviation / color coding
        y=[i['GDP per person'] for i in indicators],
    )

    life_expectancy = go.Bar(
        x=countries, #should look for flag / abbreviation / color coding
        y=[i['Healthy life expectancy'] for i in indicators],
    )

    layout = dict(
        height=350,
        template='plotly_dark',
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
    )

    fig = tools.make_subplots(rows=1, cols=3, subplot_titles=('Happiness score','GDP per person', 'Healthy life expectancy'))

    fig.append_trace(happiness_score, 1, 1)
    fig.append_trace(gdp, 1, 2)
    fig.append_trace(life_expectancy, 1, 3)

    for trace in fig['data']:
        trace['showlegend'] = False

    fig['layout'].update(
        width=750,
        height=250,
        autosize=True, 
        template='plotly_dark',
        margin=go.layout.Margin(l=10, r=10, b=20, t=20, pad=4),
        xaxis = dict(hoverformat = '.2f'),
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
        legend=None)

    return fig

