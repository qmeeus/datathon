import base64
import dash_html_components as html
import dash_core_components as dcc

from utils import absolute_path, encode_image, encode_css
from map import make_static_map
from graphs import get_kmeans_figure


preselect_countries = [
    'Belgium',
    'Romania',
    'Hungary',
]

def make_layout(*all_options):

    categories, countries, population_classes = all_options

    def dropdown(props):
        style = {
            'background-color': '#191A1A',
            'color': 'white',
            **props.get('style', {}),
        }
        kwargs = props.copy()
        kwargs['style'] = style
        assert 'id' in kwargs
        if 'options' in kwargs:
            kwargs['options'] = [{'label': k, 'value': k} for k in kwargs["options"]]
        return dcc.Dropdown(**kwargs)

    def tab_container(left, right):
        return html.Div(
            [left, right], 
            style={'display': 'flex', 'flex-direction': 'row', 'width': '100%'})

    def make_tab(children, props):
        left, right = children
        right = html.Div([html.H4(props['label'].title()), right], style={'width': '100%', 'padding': '0 10px'})
        return dcc.Tab(
            label=props['label'],
            value=props['id'],
            children=tab_container(left, right),
            style=tabStyle,
            selected_style=tabSelectedTab)

    levels = []
    for i in range(2):
        props = dict(
            id="level{}-dropdown".format(i+1), 
            placeholder='Select a category from level {}'.format(i+1),
            multi=False,
            disabled=bool(i),
            style={
                'width': '300px'
            }
        )
        if i == 0:
            props['options'] = categories
            props['value'] = list(categories)[0]
        levels.append(dropdown(props))

    country_selectors = []
    for i in range(3):
        props = dict(
            id="country{}-dropdown".format(i+1), 
            placeholder='Select a country',
            multi=False, 
            options=countries,
            value=preselect_countries[i],
        )
        country_selectors.append(html.Div([
            html.Div(dropdown(props), style={
                    'height': '50px',
                    'width': '80%',
                }),
            html.Div(id=f'country{i+1}-topfive')
        ], style={'width': '100%'}))

    menu_2_styles = {
        "margin-top": "20",
        "margin-bottom": "20",
        'display': 'flex',
        'flex-direction': 'row',
        'justify-content': 'space-around',
    }
    menu_2 = html.Div(levels, style=menu_2_styles, className="row")

    menu1 = html.Div(country_selectors, style={
        'display': 'flex',
        'flex-direction': 'row',
        'justify-content': 'space-between',
    })

    meta = dcc.Graph(id='meta-graph')

    graphs = html.Div([dcc.Graph(id='hbar-graph')], style={"margin-right": "20"})

    tabStyle = {
        'background-color': '#191A1A',
        'border': 'none',
        'padding': '0',
        'width': '150px'}

    tabSelectedTab = {
        'background-color': '#191A1A',
        'border': 'none',
        'padding': '0',
        'width': '150px',
        'border-top': '2px solid #4D6969'}

    layout = html.Div([
        html.Link(
            rel='stylesheet',
            href=f"data:text/css;base64,{encode_css('static/styles.css')}"),
                    html.Div(
            html.H3('DATANUANCERS', style={
                'font-family': 'AvenirNextCondensed-HeavyItalic',
                'color': 'rgb(77,105,105)',}),),
        dcc.Tabs([
            make_tab(
                [dcc.Graph(id='map-graph-2'), html.Div([menu_2, graphs])], 
                {'label': 'Consumption report', 'id': 2}),
            make_tab(
                [
                    dcc.Graph(id='map-graph-3', figure=make_static_map()), 
                    html.Div([dcc.Graph(id='clusterplot', figure=get_kmeans_figure(5))])],
                {'label': 'Cluster analysis', 'id': 3}),
            make_tab(
                [dcc.Graph(id='map-graph-1'), html.Div([menu1, meta])], 
                {'label': 'Country comparison', 'id': 1}),

            ], value=1, id='tabs')], 
            style={"margin": "30px"})

    return layout
