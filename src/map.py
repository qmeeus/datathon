from plotly import graph_objs as go
from plotly.graph_objs import layout as go_layout
from graphs import build_map
from utils import load_text
from data import data


mapbox_access_token = load_text("apikey")
cluster_colors = ["#D07A44", "#AE5663", "#89C0BC", "#3A8994", "#3A8994"]

def make_static_map():
    return update_graph(cluster_coloring)()

def cluster_coloring():
    countries, clusters = data.get_kmeans_country_label(5)
    country_colors = dict()
    for c, m in zip(countries, clusters):
        country_colors[c] = cluster_colors[m]
    return country_colors

def consumption_coloring(level1_value, level2_value):
    category = level2_value if level2_value else level1_value
    level = 2 if level2_value else 1

    y_values, x_values = data.get_country_mean(level, category)

    max_mean = max(x_values)
    x_norm = [(m * 1.0) / max_mean for m in x_values]
    country_colors = {}
    for c, m in zip(y_values, x_norm):
        m = 1.0 - m
        col_r = int(m * 255)
        col_g = int(m * 200)+55
        col_b = int(m * 200)+55
        country_colors[c] = f'rgba({col_r},{col_g},{col_b},0.8)'
    return country_colors

def country_highlight_coloring(*countries_to_highlight):
    country_colors = {}
    for country in countries_to_highlight:
        if country is not None:
            country_colors[country] = f'rgba(77,105,105,0.8)'
    return country_colors

def update_graph(mapcolor_fun):
    def inner(*args):
        layout = go.Layout(
            # autosize=True,
            width=400,
            height=600,
            margin=go_layout.Margin(l=0, r=0, t=0, b=0),
            hovermode='closest',
            mapbox=dict(
                layers=build_map(mapcolor_fun(*args)),
                style='dark',
                accesstoken=mapbox_access_token,
                bearing=0,
                center=dict(
                    lat=53.8097232,
                    lon=13.6563332
                ),
                pitch=0,
                zoom=2.5))

        fig = dict(data=[go.Scattermapbox()], layout=layout)

        return go.Figure(**fig)
    return inner
