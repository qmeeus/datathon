import os
import dash
from dash.dependencies import Input, Output, State
from plotly import graph_objs as go
from plotly.graph_objs import layout as go_layout
import dash_html_components as html

from data import data
from layout import make_layout
from utils import load_json
from map import update_graph, consumption_coloring, country_highlight_coloring, cluster_coloring
from graphs import update_barh, meta_graph

food_category_shorthands = {
  "Alcoholic beverages": "🍾 Alcoholic beverages",
  "Animal and vegetable fats and oils": "🛢 Fats and oils",
  "Composite food (including frozen products)": "🥫 Composite food",
  "Drinking water (water without any additives except": "💧 Drinking water",
  "Eggs and egg products": "🥚 Eggs and egg products",
  "Fish and other seafood (including amphibians, rept": "🐟 Fish and other seafood",
  "Food for infants and small children": "🍼 Food for infants",
  "Fruit and fruit products": "🍇 Fruit and fruit products",
  "Fruit and vegetable juices": "🍹 Fruit and vegetable juices",
  "Grains and grain-based products": "🌽 Grains and grain-based...",
  "Herbs, spices and condiments": "🌱 Herbs, spices and condiments",
  "Legumes, nuts and oilseeds": "🥜 Legumes, nuts and oilseeds",
  "Meat and meat products (including edible offal)": "🥩 Meat and meat products",
  "Milk and dairy products": "🧀 Milk and dairy products",
  "Non-alcoholic beverages (excepting milk based beve": "🥤 Non-alcoholic beverages",
  "Products for special nutritional use": "💪 Products for special nutritional use",
  "Snacks, desserts, and other foods": "🍫 Snacks, desserts, and other foods",
  "Starchy roots and tubers": "🥕 Starchy roots and tubers",
  "Sugar and confectionary": "🍬 Sugar and confectionary",
  "Vegetables and vegetable products (including fungi": "🥦 Vegetables",
}

def main(server=None):
    app = dash.Dash(__name__, server=server)
    # app.config['suppress_callback_exceptions'] = True
    app.css.append_css({
        'external_url': 'https://cdn.rawgit.com/chriddyp/ca0d8f02a1659981a0ea7f013a378bbd/raw/e79f3f789517deec58f41251f7dbb6bee72c44ab/plotly_ga.js'})

    categories = load_json("../data/categories.json")
    countries = list(data.get_sheet_by_id(1)["Country"].unique())
    population_classes = list(data.get_sheet_by_id(1)["Pop Class"].unique())

    app.layout = make_layout(categories, countries, population_classes)

    for i in range(1, 4):
        @app.callback(
            Output(f'country{i}-topfive', 'children'),
            [Input(f'country{i}-dropdown', 'value')])
        def update_topfive(country):
            top_five = data.get_country_top_cat(country)
            return html.Ol([html.Li(food_category_shorthands.get(c, c)) for c in top_five])

    @app.callback(
        [Output('level2-dropdown', prop) for prop in ('options', 'disabled')],
        [Input('level1-dropdown', 'value')])
    def set_level2_options(selected_level1):
        if selected_level1:
            options = list(categories[selected_level1])
            return [{'label': i, 'value': i} for i in options], False
        else:
            return [], True

    app.callback(Output("hbar-graph", "figure"),
        [Input("level1-dropdown", "value"), 
        Input("level2-dropdown", "value")])(update_barh)

    # for i, o1, o2 in zip([1,2,3], [2,3,1], [3,1,2]):
    #     @app.callback(
    #         [Output(f"country{o}-dropdown", "options") for o in (o1, o2)],
    #         [Input(f"country{i}-dropdown", "value")])
    #     def update_country_dropdown(country_name):
    #         country_name = country_name if type(country_name) == str else country_name[0]
    #         countries = data.get_country_name()
    #         countries.remove(country_name)
    #         return countries, countries

    app.callback(
        Output('meta-graph', 'figure'),
        [Input(f'country{i}-dropdown', 'value') for i in range(1, 4)]
    )(meta_graph)

    app.callback(Output("map-graph-1", "figure"),
        [Input(f'country{i}-dropdown', 'value') for i in range(1, 4)],
        )(update_graph(country_highlight_coloring))

    app.callback(Output("map-graph-2", "figure"),
        [Input(f"level{i}-dropdown", "value") for i in (1,2)],
        )(update_graph(consumption_coloring))

    app.callback(Output("map-graph-3", "figure"),[])(update_graph(cluster_coloring))

    external_css = [
        '/static/stylesheet.css',
        "https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
        "//fonts.googleapis.com/css?family=Raleway:400,300,600",
        "//fonts.googleapis.com/css?family=Dosis:Medium",
        "https://cdn.rawgit.com/plotly/dash-app-stylesheets/0e463810ed36927caf20372b6411690692f94819/dash-drug-discovery-demo-stylesheet.css",
        "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
        "https://codepen.io/chriddyp/pen/brPBPO.css",  # handles loading delay
    ]

    for css in external_css:
        app.css.append_css({"external_url": css})

    return app

if __name__ == '__main__':
    app = main()
    app.run_server(debug=True, port=os.environ["PORT"])
