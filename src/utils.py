import os.path as p
import json
import base64


def absolute_path(path_from_root):
    return p.abspath(p.join(p.dirname(__file__), path_from_root))


def load_json(path_from_root):
    with open(absolute_path(path_from_root)) as json_file:
        return json.load(json_file)

def encode_image(path_from_root):
    with open(absolute_path(path_from_root), 'rb') as img:
        encoded_image = base64.b64encode(img.read())
    return encoded_image.decode()

def encode_css(path_from_root):
    with open(absolute_path(path_from_root), 'r') as css:
        encoded_image = base64.b64encode(css.read().encode('utf-8'))
    return encoded_image.decode()

def load_text(path_from_root):
    with open(absolute_path(path_from_root)) as f:
        return f.read().strip()
