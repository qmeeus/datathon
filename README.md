# KU Leuven datathon

Website: [https://data-nuancers.herokuapp.com/](https://data-nuancers.herokuapp.com/)

## Ideas
 - Build a tool to allow a user to enter some personal data and compare his food consumption with European Countries
 - Health approach and cross with data from WHO to do some predictive / prescriptive analysis 
    - risky because food is only one factor among many to explain health situation
 - Take an environmental approach and cross data with econometric data 
    - Imports / Exports to highlight what is produced in the country
    - CO2 and pollutant emissions by industry to highlight what is produced in a environment-friendly way
 - Map the differences in terms of food consumption between countries (see the website provided by Matyas), in the links below

## Data
 - [EU Data Portal](http://data.europa.eu/euodp/en/data/)
 - [Money spent on food/alcohol per country](https://www.ers.usda.gov/topics/international-markets-us-trade/international-consumer-and-food-industry-trends/#data)
 - [Database of agriculture in EU](https://ec.europa.eu/eurostat/web/agriculture/data/database)

## Inspiration
 - [Dash Gallery](https://dash.plot.ly/gallery)
 - [Simple food dashboard of National Geographic](https://www.nationalgeographic.com/what-the-world-eats/)
 - [Mapping EFSA's food consumption data with tmap](https://behrica.github.io/blog/2017/10/food-consumption-tutorial.html)
 - [A presentation of the EFSA's food consumption database](https://www.bfr.bund.de/cm/349/efsas_concise_european_food_consumption_database.pdf)

## Docker
Docker is a computer program that performs operating-system-level virtualization also known as containerization. Without entering too much into details, when using docker, an app is deployed in its own container that is completely isolated from the system. That means that there is no problem of compatibility (Operating Systems, mismatch in libraries versions, etc.). The container is almost always based on some flavour of linux and the linux principles apply (file permission etc.).

A docker container is build using a Dockerfile. Typically, it states the container it is based on (here python:3.6.5), installs the requirements, and defines how to start the code. In short, one needs first to build the container, then run it for the first time. After that, you can stop and start a container by using simple commands such as `docker stop` and `docker start`, and inspect the logs by using `docker logs -f [container-name]`

To build the container:

`docker build -f Dockerfile -t dash --build-arg user_id=(id -u) .`

To run the container:

`docker run -d --name dashboard --network=host -v (pwd):/home/patrick/src dash`

To inspect the logs:

`docker logs -f dashboard`

The app runs on [http://localhost:8050/](http://localhost:8050/)